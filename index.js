var request = require('request');

let albumNumber = 0;
albumNumber = process.argv[2];

if(process.argv.length!=3){
    console.log('Invalid Number of Parameters')
    process.exit(1)
}

if(isNaN(albumNumber/1)){
    console.log('Paramter must be nubmer')
    process.exit(1)
}

request('https://jsonplaceholder.typicode.com/photos?albumId='+albumNumber, function(error, response, body){
    let albums = JSON.parse(body);
    for(let i=0;i<albums.length;i++){
        console.log('['+albums[i].id+'] '+albums[i].title)
    }
})